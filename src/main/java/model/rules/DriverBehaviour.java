package model.rules;

public interface DriverBehaviour {

    /**
     * update vehicles with rule's action.
     */
    void roadSight();
}
