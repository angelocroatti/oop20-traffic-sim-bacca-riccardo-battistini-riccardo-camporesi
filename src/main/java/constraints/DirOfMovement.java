package constraints;

public enum DirOfMovement {

    /**
     * 
     */
    NORTH_SOUTH,

    /**
     * 
     */
    SOUTH_NORTH,

    /**
     * 
     */
    EAST_WEST,

    /**
     * 
     */
    WEST_EAST;

}
